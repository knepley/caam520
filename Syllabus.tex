\documentclass[]{article}

\newcommand{\red}{\textcolor{red}}

\usepackage[
    %pdftex,
    pdftitle={Lecture Notes},
    pdfauthor={Matthew G.~Knepley},
    pdfpagemode={UseOutlines},
    bookmarks, bookmarksopen, bookmarksnumbered={True},
    colorlinks, linkcolor={blue}, citecolor={blue}, urlcolor={blue}
    ]{hyperref}

\title{CAAM 520: Computational Science II\\Spring 2017 Syllabus}
\author{Matthew G. Knepley\\3021 Duncan Hall \qquad \href{mailto:knepley@rice.edu}{knepley@rice.edu}}
\date{}

\begin{document}
\maketitle

This course concerns \textit{scientific libraries} as the most effective form of communication for advances in scientific
computation. We will learn how to design, implement, test, distribute, and maintain a numerical library for finite
element (FEM) computation written in a higher level languages, such as C, Fortran, and Python. Emphasis will be placed
solving practical computational problems and providing insight to the user on code accuracy, performance, and
tradeoffs. We will also cover basic techniques of algorithm design and implementation, architectural optimization,
source management, configuration and build tools, documentation, i/o, and visualization.

Students will learn to write software as a library developer, meaning that they will construct software usable by
others. This is a crucial skill for modern computational science, which needs an understandable (modular, hierarchical),
extensible, maintainable infrastructure.

\section{Course Information}

\begin{tabular}{ll}
{\bf Instructor}  & \href{http://www.caam.rice.edu/~mk51}{Matthew G. Knepley} \\
{\bf Class times} & 1pm to 1:50pm on Monday, Wednesday, \& Friday \\
{\bf Location}    & Duncan Hall 1075
\end{tabular}

\bigskip

A \href{http://www.caam.rice.edu/~caam520/CourseOverview.pdf}{course overview} and \href{http://www.caam.rice.edu/~caam520/GradingPolicy.pdf}{grading policy} are available in accordance with Rice academic policy.

\section{Required and Recommended Reading}

Most class time will be occupied by collaborative learning, question and answer time, and group
activity. \href{http://www.caam.rice.edu/~caam519/CSBook.pdf}{Class notes} for CAAM 519 provide background material for
this course.

\bigskip

The mathematics textbook to rule them all for FEM is
\begin{itemize}
  \item \href{https://www.amazon.com/Mathematical-Element-Methods-Applied-Mathematics/dp/0387759336/ref=sr_1_1?ie=UTF8&qid=1481575954&sr=8-1&keywords=brenner+scott}{The Mathematical Theory of Finite Element Methods} by Susanne Brenner and L. Ridgway Scott (Springer) ISBN-10: 0387759336
\end{itemize}
but some other books which could supplement your reading are
\begin{itemize}
  \item \href{https://www.amazon.com/Numerical-Nonlinear-Differential-Computational-Mathematics/dp/3319137964/ref=sr_1_fkmr0_2?ie=UTF8&qid=1481576113&sr=8-2-fkmr0&keywords=soren+bartels+fem}{Numerical Methods for Nonlinear Partial Differential Equations} by S\"oren Bartels (Springer) ISBN-10: 3319137964
  \item \href{https://www.amazon.com/Understanding-Implementing-Finite-Element-Method/dp/0898716144/ref=sr_1_fkmr0_2?ie=UTF8&qid=1481576052&sr=8-2-fkmr0&keywords=gockenbach+fem}{Understanding And Implementing the Finite Element Method} by Mark S. Gockenbach (SIAM) ISBN-10: 0898716144
  \item \href{https://www.amazon.com/Finite-Elements-Fast-Iterative-Solvers/dp/0198528671}{Finite Elements and Fast
    Iterative Solvers: with Applications in Incompressible Fluid Dynamics} by Howard C. Elman, David J. Silvester, and
    Andrew J. Wathen (Oxford). ISBN-10: 0198528671
\end{itemize}

\bigskip

The following books are foundational for the software construction, however we will not follow them in any way, and they are intended for reference and outside reading.
\begin{itemize}
  \item \href{https://www.amazon.com/Programming-Language-Brian-W-Kernighan/dp/0131103628/ref=sr_1_1?s=books&ie=UTF8&qid=1481576301&sr=1-1&keywords=the+c+programming+language}{The C Programming Language} (2nd Edition) by Brian W. Kernighan and Dennis M. Ritchie (Prentice Hall) ISBN-10: 0131103628.

  \item \href{http://www.mcs.anl.gov/petsc/petsc-current/docs/manual.pdf}{The PETSc Manual}, S. Balay, S. Abhyankar, M. Adams, J. Brown, P. Brune, K. Buschelman, V. Eijkhout, W. Gropp, D. Kaushik, M. Knepley, L. Curfman McInnes, K. Rupp, B. Smith, and H. Zhang, ANL-95/11 Rev 3.5, 2015.

  \item \href{http://www.amazon.com/LaTeX-Companion-Techniques-Computer-Typesetting/dp/0201362996/ref=sr_1_1?s=books&ie=UTF8&qid=1439318288&sr=1-1&keywords=latex+companion}{LaTex Companion} (Tools and Techniques for Computer Type Setting), 2nd ed., by Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, and Chris Rowley (Addison Wesley Professional) ISBN-10: 0201362996.
\end{itemize}

\section{Schedule}

%1 Decoupling of Discretization and Equation Specification (5 weeks) 15 (13) Lectures
%  - Present cell Residual evaluation 11 Lectures
%    - FEM Formulation 1 Lecture
%    - FIAT Formulation 1 Lecture
%    - Plex basics 1 Lecture
%    - Residual Formulation 2 Lectures
%    - Jacobian Formulation 1 Lecture
%    - Boundary Conditions 2 Lectures
%    - Build ex12 from scratch 3 lectures
%  - Implement other elements and other equations and MMS 2 Lectures
%    - Elements (Nedelec, DG, RT, Interface FInAT)
%    - Equations (Helmholtz, VV Stokes + Energy, Magma, MHD)
%    - Groups present work 2 Lectures
%
%2 Architectural Optimization (3 weeks) 9 (8) Lectures
%  - Vectorization Strategies 1 Lecture
%  - FEM kernel paper from arXiv 2 Lectures
%  - Particle interaction 1 Lecture
%  - Groups present reimplementation for Xeon and Xeon Phi with perf timing 2 Lectures
%  - Cache Tiling 1 Lecture
%  - Current chunking that I do for residual eval 1 Lecture
%
%3 Solving Real Problems (5 weeks) 15 (14) Lectures
%  - Choose problems at start
%  - Multilevel Considerations for Preconditioning 1 Lecture
%  - Diskin MG Debugging 1 Lecture
%  - Parallel Considerations for Preconditioning 1 Lecture
%  - Profiling Real Codes 2 Lectures
%  - FEM Pitfalls from Dave May review 1 Lecture
%  - Problem formulations 2 Lectures
%  - Prospective solvers 3 Lectures
%  - Implement real solver
%    - VV Stokes
%    - Poisson with nonlinear Robin condition
%    - Darcy Problem?
%    - Helmholtz?
%    - Work-precision discrimination
%  - Groups present solver 3 Lectures

\subsection{Week 1: Introduction to FEM 1/9}

\subsubsection{Lecture 1: Course Introduction}

The paper is the perfect vehicle for mathematical communication, but not for communication of results in scientific
computing. In this arena, it is the numerical library. I will discuss the creation of software libraries.

\bigskip

\noindent I will also the three parts of the course, and the associated programming project for each.

\bigskip

\noindent Online resources
\begin{itemize}
  \item \href{http://arxiv.org/abs/1407.2905}{Run-time extensibility and librarization of simulation software}, Jed Brown, Matthew G. Knepley, and Barry F. Smith, IEEE CSE, 17(1), 38-45, 2015.
  \item Short talk on \href{http://www.caam.rice.edu/~mk51/presentations/PrepLibraryDev.pdf}{Library development}
\end{itemize}


\subsubsection{Lecture 2: Read up on FEM}

I am out of town.

\subsubsection{Lecture 3: FEM Basics}

I go over simple PDE formulations and the finite element method.

\bigskip

\noindent Online resources
\begin{itemize}
  \item \href{http://epubs.siam.org/doi/abs/10.1137/100804036}{From Euler, Ritz, and Galerkin to Modern Computing},
    Martin Gander, SIAM Review, 54(4), 627--666, 2012.
\end{itemize}

\subsection{Week 2: FEM Tools 1/16}

\subsubsection{Lecture 4: Holiday: No Class}

Read up on FIAT.

\bigskip

\noindent Online resources
\begin{itemize}
  \item \href{https://fenics.readthedocs.io/projects/fiat/en/latest/}{Online documentation} for FIAT, as part of the \href{https://fenicsproject.org/}{FEniCS Project}
  \item \href{http://dl.acm.org/citation.cfm?doid=1039813.1039820}{Algorithm 839: FIAT, a new paradigm for computing
    finite element basis functions}, ACM TOMS, {\bf 30}(4), 502--516, 2004. [\href{http://104.130.4.253/compendia/2013.377/}{Compendia}]
  \item \href{http://epubs.siam.org/doi/pdf/10.1137/15M1021167}{Automated generation and symbolic manipulation of tensor product finite elements}, SIAM SISC, {\bf 38}(5), S25--S47, 2016. [\href{https://arxiv.org/abs/1411.2940}{arXiv}]
\end{itemize}

\subsubsection{Lecture 5: FIAT}

I introduce the FIAT and FInAT packages and the computational approach to element development.

\bigskip

\noindent \red{I describe the first programming project.}

\subsubsection{Lecture 6: Plex basics}

I introduce the Plex framework for mesh representation (see Chapter 7 in CAAM 519 book).

\subsection{Week 3: Residuals and Jacobians 1/23}

\subsubsection{Lecture 7: Residual Calculation I}

We look at residual calculation on an element, and pointwise physics functions.

\bigskip

\noindent Online resources
\begin{itemize}
  \item \href{https://arxiv.org/abs/1309.1204}{Achieving High Performance with Unified Residual Evaluation}
  \item \href{https://arxiv.org/abs/1103.0066}{Finite Element Integration on GPUs}
  \item \href{https://arxiv.org/abs/1607.04245}{Finite Element Integration with Quadrature on the GPU}
  \item \href{http://www.sciencedirect.com/science/article/pii/S0029549309002635}{MOOSE: A parallel computational framework for coupled systems of nonlinear equations}
\end{itemize}

\subsubsection{Lecture 8: Residual Calculation II}

We look at residual calculation over a domain.

\subsubsection{Lecture 9: Element Discussion}

\red{We discuss progress on the first programming project.}

\subsection{Week 4: Boundary Conditions 1/30}

\subsubsection{Lecture 10: Jacobian Calculation}

We look at Jacobian calculation over an element.

\subsubsection{Lecture 11: Boundary Conditions I}

We discuss handling boundary conditions with PetscSection (see Chapter 7.2.2 in the CAAM 519 book).

\subsubsection{Lecture 12: Boundary Conditions II}

We assemble full solutions and local solutions with boundary values.

\subsection{Week 5: Project Presentations 2/6}

\subsubsection{Lecture 13: Project Presentation I}

\red{Students present their first programming project for a new element.}

\subsubsection{Lecture 14: Project Presentation II}

\red{Students present their first programming project for a new element.}

\subsubsection{Lecture 15: Holiday: No Class}

\subsection{Week 6: Building a Simulation From Scratch 2/13}

\subsubsection{Lecture 16: SNES ex12 I}

We create \href{http://www.mcs.anl.gov/petsc/petsc-current/src/snes/examples/tutorials/ex12.c.html}{SNES ex12} from scratch.

\subsubsection{Lecture 17: SNES ex12 II}

We create \href{http://www.mcs.anl.gov/petsc/petsc-current/src/snes/examples/tutorials/ex12.c.html}{SNES ex12} from scratch.

\subsubsection{Lecture 18: SNES ex12 III}

We create \href{http://www.mcs.anl.gov/petsc/petsc-current/src/snes/examples/tutorials/ex12.c.html}{SNES ex12} from scratch.

\subsection{Week 7: Vectorization 2/20}

\subsubsection{Lecture 19: Vectorization}

I introduce vectorization and technologies for enabling it.

\bigskip

\noindent \red{I describe the second programming project.}

\subsubsection{Lecture 20: Vectorizing FEM I}

We discuss vectorization of finite element integrals.

Online resources
\begin{itemize}
  \item \href{https://arxiv.org/abs/1607.04245}{Finite Element Integration with Quadrature on the GPU}
  \item \href{https://www.khronos.org/opencl/}{OpenCL} is an open standard for SIMT programming, used extensively by
    \href{http://viennacl.sourceforge.net/}{ViennaCL} and \href{https://mathema.tician.de/software/pyopencl/}{PyOpenCL}
  \item \href{https://en.wikipedia.org/wiki/CUDA}{CUDA} is the SIMT programming system from Nvidia, which is easily used
    through \href{https://mathema.tician.de/software/pycuda/}{PyCUDA}
  \item \href{http://ispc.github.io/}{ISPC} is a compiler that inserts Intel intrinsics (also look at non-temporal stores)
  \item \href{https://github.com/hfp/libxsmm}{LIBXSMM} is a library with Intel JIT compilation for small, dense or sparse matrix multiplications, and small convolutions.
  \item Example
    \href{https://bitbucket.org/petsc/petsc/src/492360bb5720a1b2a727b93ba3ae980ae135c162/src/dm/dt/interface/dtfe.c?at=master&fileviewer=file-view-default#dtfe.c-5011}{PETSc
    integration} using OpenCL
\end{itemize}

\subsubsection{Lecture 21: Vectorizing FEM II}

We run some experiments.

\subsection{Week 8: Self-Study 2/27}

\subsubsection{Lecture 22: Conference: No Class}

I am out of town.

\subsubsection{Lecture 23: Conference: No Class}

I am out of town.

\subsubsection{Lecture 24: Conference: No Class}

I am out of town.

\subsection{Week 9: Code Optimization 3/6}

\subsubsection{Lecture 25: Cache Tiling}

I describe cache tiling (also \href{https://en.wikipedia.org/wiki/Loop_tiling}{loop tiling}) and show code for this in PETSc.

Online resources
\begin{itemize}
  \item \href{https://mathema.tician.de/software/loopy/}{Loopy}, an iteration layout engine, from \href{https://mathema.tician.de/}{Andreas Klockner}
  \item The STENCIL computation in the \href{http://impact.crhc.illinois.edu/parboil/parboil.aspx}{Parboil benchmarks}
    from UIUC shows off cache tiling
  \item Another approach to this is \href{https://en.wikipedia.org/wiki/Cache-oblivious_algorithm}{cache oblivious
    algorithms}, originated by Harald Prokop in his \href{http://supertech.csail.mit.edu/papers/Prokop99.pdf}{MS
    thesis}, where stuff is recursively chopped up until it fits in cache.
\end{itemize}

\subsubsection{Lecture 26: Project Presentation I}

\red{Students present their second programming project on vectorization.}

\subsubsection{Lecture 27: Project Presentation II}

\red{Students present their second programming project on vectorization.}

\subsection{Week 10: Spring Break 3/13}

\subsubsection{Lecture 28: Holiday: No Class}

\subsubsection{Lecture 29: Holiday: No Class}

\subsubsection{Lecture 30: Holiday: No Class}

\subsection{Week 11: Problem Formualtions 3/20}

\subsubsection{Lecture 31: Irregular Evaluation}

We examine the irregular evaluation needed for particle methods, and look at the design of DMSwarm.

\subsubsection{Lecture 32: Multiphysics Formulations}

We discuss
\begin{itemize}
  \item Poisson with a nonlinear boundary condition
  \item Variable-viscosity Stokes and Navier-Stokes
  \item Helmholtz
  \item Magneto-hydrodynamics
  \item Magama dynamics
\end{itemize}

\subsubsection{Lecture 33: Group Projects}

\red{We choose formulations for the third programming project, and divide into groups.}

\subsection{Week 12: Solver Design 3/27}

\subsubsection{Lecture 34: Multilevel Preconditioning}

We discuss how to design multigrid preconditioners

\subsubsection{Lecture 35: Debugging Multigrid}

We discuss the paper by Diskin, Thomas, and Mineck on debugging a multigrid implementation.

Online resources
\begin{itemize}
  \item \href{http://epubs.siam.org/doi/abs/10.1137/030601521?journalCode=sjoce3}{On Quantitative Analysis Methods for
    Multigrid Solutions}, SIAM SISC, {\bf 27}(1), 108--129, 2005.
\end{itemize}

\subsubsection{Lecture 36: Parallel Preconditioning}

We discuss parallel considerations when preconditioning.

\subsection{Week 13: Quantifying Codes 4/3}

\subsubsection{Lecture 37: FEM Pitfalls}

We discuss the paper review by Dave May illustrating common pitfalls in FEM programming. We also discuss solver issues
for group projects.

Online resources
\begin{itemize}
  \item \href{http://www.solid-earth-discuss.net/6/C877/2014/sed-6-C877-2014.pdf}{Interactive comment on ``ELEFANT: a user-friendly
multipurpose geodynamics code''}, Solid Earth Discussions, {\bf 6}, C877--C893, 2014.
\end{itemize}

\subsubsection{Lecture 38: Simple Profiling}

I discuss the PETSc profiling interface.

\subsubsection{Lecture 39: Profiling Real Codes}

We discuss how to extract actionable information from complex, practical codes, rather than toy problems. Also, we
discuss work-precision discrimination.

\subsection{Week 14: Group Work 4/10}

\subsubsection{Lecture 40: Group Project Work}

I am out of town.

\subsubsection{Lecture 41: Group Project Work}

I am out of town.

\subsubsection{Lecture 42: Group Project Work}

I am out of town.

\subsection{Week 15: Final Evaluation 4/17}

\subsubsection{Lecture 43: Project Presentation I}

\red{Student groups present their third programming project.}

\subsubsection{Lecture 44: Project Presentation II}

\red{Student groups present their third programming project.}

\subsubsection{Lecture 45: Class Evaluation}

We discuss class accomplishments and possible improvements.

\end{document}
