PRES_FIGS = /PETSc3/presentations-git/figures
ALLPDF    = CourseOverview.pdf GradingPolicy.pdf Syllabus.pdf CourseProjects.pdf
ALLHTML   = Syllabus.html Syllabus.css

all: ${ALLPDF} ${ALLHTML} LectureNotes.pdf

clean::
	-@rm -f ${ALLPDF} *.aux *.bbl *.blg *.fdb_latexmk *.fls *.log *.out *.run.xml *.toc
	-@rm -f ${ALLHTML} *.4ct *.4tc *.idv *.lg *.tmp *.xref

sync:
	rsync -avz -e ssh ${ALLPDF} ${ALLHTML} index.html caam:/www/caam520-S16/public_html

%.pdf: %.tex
	TEXINPUTS=${TEXINPUTS}:${PRES_FIGS} BIBINPUTS=.:/PETSc3/papers/bibtexjb:${PETSC_DIR}/src/docs/tex:/Users/knepley/Documents latexmk -pdf $^

%.html: %.tex
	htlatex $^ "xhtml,charset=latin1"

include ${PETSC_DIR}/lib/petsc/conf/rules
include ${PETSC_DIR}/lib/petsc/conf/variables
