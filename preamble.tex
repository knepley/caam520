\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{bbm}
\usepackage{graphicx}
\usepackage{multirow}
\usepackage{dtklogos}
\usepackage{wrapfig}
\usepackage{csquotes}
\usepackage{upquote}

% hyperref package should always be loaded as the very last one
% to be sure that it has the last word ...
\usepackage[
    pdftex,
    pdftitle={Lecture Notes},
    pdfauthor={Matthew G.~Knepley},
    pdfpagemode={UseOutlines},
    bookmarks, bookmarksopen, bookmarksnumbered={True},
    colorlinks, linkcolor={blue}, citecolor={blue}, urlcolor={blue}
    ]{hyperref}

%% Define a new style for the url package that will use a smaller font.
\makeatletter
\def\url@leostyle{%
  \@ifundefined{selectfont}{\def\UrlFont{\sf}}{\def\UrlFont{\footnotesize\sffamily}}}
\makeatother
%% Now actually use the newly defined style.
\urlstyle{leo}

% TikZ
\usepackage{tikz}
\usepackage{pgflibraryshapes}
\usetikzlibrary{decorations.text}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{backgrounds}
\usetikzlibrary{calc}
\usetikzlibrary{arrows}
\usetikzlibrary{snakes}
\usetikzlibrary{positioning}
%%\input{figures/tikzColors}
\definecolor{cffffff}{RGB}{255,255,255}
\definecolor{cff00c0}{RGB}{255,0,192}
\definecolor{c0f00ff}{RGB}{15,0,255}
\definecolor{c00ff20}{RGB}{0,255,32}
\definecolor{cfffd00}{RGB}{255,253,0}
\definecolor{c00e0ff}{RGB}{0,224,255}
\usepackage{pgfplots}
\usepackage{pgfplotstable}

% Color macros
\newcommand{\black}{\textcolor{black}}
\newcommand{\blue}{\textcolor{blue}}
\newcommand{\green}{\textcolor{green}}
\newcommand{\red}{\textcolor{red}}
\newcommand{\brown}{\textcolor{brown}}
\newcommand{\cyan}{\textcolor{cyan}}
\newcommand{\magenta}{\textcolor{magenta}}
\newcommand{\yellow}{\textcolor{yellow}}

\usepackage{color}
\definecolor{deepblue}{rgb}{0,0,0.5}
\definecolor{deepred}{rgb}{0.6,0,0}
\definecolor{deepgreen}{rgb}{0,0.5,0}

% Default fixed font does not support bold face
\DeclareFixedFont{\ttb}{T1}{cmtt}{bx}{n}{10} % for bold
\DeclareFixedFont{\ttm}{T1}{cmtt}{m}{n}{10}  % for normal
\DeclareFixedFont{\sttb}{T1}{cmtt}{bx}{n}{8} % for bold
\DeclareFixedFont{\sttm}{T1}{cmtt}{m}{n}{8}  % for normal
% Want a lowercase calligraphic math
\DeclareMathAlphabet{\mathpzc}{OT1}{pzc}{m}{it}
% Font for lstlisting
% \usepackage{courier}

\def\clopt#1{{\tt #1}}
\def\PetscEvent#1{{\tt #1}}

% Vectors
\newcommand{\vF}{\Vec{F}}
\newcommand{\vJ}{\Vec{J}}
\newcommand{\vM}{\Vec{M}}
\newcommand{\vN}{\Vec{N}}
\newcommand{\vU}{\Vec{U}}
\newcommand{\vb}{\Vec{b}}
\newcommand{\vd}{\Vec{d}}
\newcommand{\vf}{\Vec{f}}
\newcommand{\vj}{\Vec{j}}
\newcommand{\vk}{\Vec{k}}
\newcommand{\vn}{\Vec{n}}
\newcommand{\vr}{\Vec{r}}
\newcommand{\vu}{\Vec{u}}
\newcommand{\vv}{\Vec{v}}
\newcommand{\vx}{\Vec{x}}
\newcommand{\vy}{\Vec{y}}
\newcommand{\vz}{\Vec{z}}

% Nonlinear equations
\newcommand{\F}{\mathcal{F}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\N}{\mathcal{N}}

\usepackage{listings}

% bash style for highlighting
\newcommand\bashstyle{\lstset{
language=bash,
basicstyle=\scriptsize\ttfamily,
columns=fullflexible
}}

% bash environment
\lstnewenvironment{bash}[1][]
{
\bashstyle
\lstset{#1}
}
{}

% bash for inline
\newcommand\bashinline[1]{{\bashstyle\lstinline!#1!}}


% make style for highlighting
\newcommand\makestyle{\lstset{
language=make,
basicstyle=\scriptsize\ttfamily,
columns=fullflexible
}}

% make environment
\lstnewenvironment{make}[1][]
{
\makestyle
\lstset{#1}
}
{}

% make for inline
\newcommand\makeinline[1]{{\makestyle\lstinline!#1!}}

% Python style for highlighting
\newcommand\pythonstyle{\lstset{
language=Python,
basicstyle=\sttm,
otherkeywords={self},             % Add keywords here
keywordstyle=\sttb\color{deepblue},
emph={MyClass,__init__},          % Custom highlighting
emphstyle=\sttb\color{deepred},    % Custom highlighting style
commentstyle=\sttm\color{deepgreen},
frame=tb,                         % Any extra options here
showstringspaces=false,           %
columns=fullflexible              % Necessary for proper cut/paste
}}

% Python style for inline highlighting
\newcommand\pythoninlinestyle{\lstset{
language=Python,
basicstyle=\ttm,
otherkeywords={self},             % Add keywords here
keywordstyle=\ttb\color{deepblue},
emph={MyClass,__init__},          % Custom highlighting
emphstyle=\ttb\color{deepred},    % Custom highlighting style
commentstyle=\ttm\color{deepgreen},
frame=tb,                         % Any extra options here
showstringspaces=false,           % 
columns=fullflexible              % Necessary for proper cut/paste
}}

% Python environment
\lstnewenvironment{python}[1][]
{
\pythonstyle
\lstset{#1}
}
{}

% Python for external files
\newcommand\pythonexternal[2][]{{
\pythonstyle
\lstinputlisting[#1]{#2}}}

% Python for inline
\newcommand\pythoninline[1]{{\pythoninlinestyle\lstinline!#1!}}

% C style for highlighting
\newcommand\cstyle{\lstset{
language=C,
basicstyle=\sttm,
otherkeywords={MPI_Comm,TS,SNES,KSP,PC,DM,Mat,Vec,VecScatter,IS,PetscSF,PetscSection,PetscObject,PetscInt,PetscScalar,PetscReal,PetscBool,InsertMode,PetscErrorCode}, % Add keywords here
keywordstyle=\sttb\color{deepblue},
emph={PETSC_COMM_WORLD,PETSC_NULL,SNES_NGMRES_RESTART_PERIODIC},          % Custom highlighting
emphstyle=\sttb\color{deepred},    % Custom highlighting style
commentstyle=\sttm\color{brown},
stringstyle=\sttm\color{deepgreen},
frame=tb,                         % Any extra options here
showstringspaces=false,           % 
columns=fullflexible              % Necessary for proper cut/paste
}}

\newcommand\cinlinestyle{\lstset{
language=C,
basicstyle=\ttm,
otherkeywords={MPI_Comm,TS,SNES,KSP,PC,DM,Mat,Vec,VecScatter,IS,PetscSF,PetscSection,PetscObject,PetscInt,PetscScalar,PetscReal,PetscBool,InsertMode,PetscErrorCode}, % Add keywords here
keywordstyle=\ttb\color{deepblue},
emph={PETSC_COMM_WORLD,NULL,SNES_NGMRES_RESTART_PERIODIC},          % Custom highlighting
emphstyle=\ttb\color{deepred},    % Custom highlighting style
commentstyle=\ttm\color{brown},
stringstyle=\ttm\color{deepgreen},
frame=tb,                         % Any extra options here
showstringspaces=false,           % 
columns=fullflexible              % Necessary for proper cut/paste
}}

% C environment
\lstnewenvironment{cprog}[1][]
{
\cstyle
\lstset{#1}
}
{}

% C for external files
\newcommand\cexternal[2][]{{
\cstyle
\lstinputlisting[#1]{#2}}}

% C for inline
\newcommand\cinline[1]{{\cinlinestyle\lstinline!#1!}}

% Plex oeprations
\def\cone{\mathbbm{cone}}
\def\supp{\mathbbm{supp}}
\def\cl{\mathbbm{cl}}
\def\st{\mathbbm{st}}
