\documentclass[]{article}

\input{preamble.tex}
\usepackage[sorting=none, backend=biber, style=authoryear]{biblatex}
\addbibresource{petsc.bib}
\addbibresource{petscapp.bib}

\title{CAAM 520: Computational Science II\\Spring 2017 Lecture Notes}
\author{Matthew G. Knepley\\3021 Duncan Hall \qquad \href{mailto:knepley@rice.edu}{knepley@rice.edu}}
\date{}

\begin{document}
\maketitle

\section{Lecture 1}

Scientific libraries are necessary in order to communicate algorithmic advances effectively. Too much is left
unspecified in papers, although papers are also necessary for proof convergence, accuracy, and performance
models. Anything connected with performance measurements is unreviewable without running the code.

Libraries hide both hardware details (MPI) and implementation complexity (Krylov solvers). They also allow the
accumulation of best practices without user input (default choice of classical Gram-Schmidt with selective
reorthogonalization for parallel Krylov solvers). Users can also benefit from library improvements without any changes
to their own code, such as better time step adaptivity in PETSc. Finally, extensibility is the hallmark of scientific
code, since scientists are by definition attacking problems which have not been solved previously.

Historical Numerical Libraries
\begin{itemize}
  \item[71] Handbook for Automatic Computation: Linear Algebra, J. H. Wilkinson and C. Reinch
  \item[73] EISPACK, Brian Smith et.al.
  \item[79] BLAS, Lawson, Hanson, Kincaid and Krogh
  \item[90] LAPACK, many contributors
  \item[91] PETSc, Gropp and Smith
  \item[95] MPICH, Gropp and Lusk
\end{itemize}

This class will be centered around three programming projects, two of which are individual and one group-oriented. The
individual projects will be approximately $2 \frac{1}{2}$ weeks long, and the group project $3 \frac{1}{2}$ weeks. They
will cover finite element construction, optimization, and use in science and engineering.

Emphasize that C programming and PETSc use will be extremely important. If you are weak in these areas, the class will
be oppressive.

\section{Lecture 3}

Any discussion of the finite element method should start with a search for solutions to partial differential
equations. How do we solve them (or associated eigenvalue or optimization problems)? The first thing we would love to do
is write down the solution function. This is almost never possible. However, we can do lots of things, such as infinite
series and spectral methods, Green function integrals, conformal transformations, etc. Why don't we just use these
methods of the 18th and 19th centuries? I think the main objection is that general boundary geometry becomes very
difficult. However, the study of variational problems leads to a new idea (weak solution) and a new method (Ritz method)
which happens also to solve the problem of geometrically complex boundaries.

Ritz (1908) developed a method which is based on using an ansatz for the solution
\begin{align}
  u(x) = \sum_i u_i \phi_i(x)
\end{align}
so that the continuum problem reduces to a finite dimensional problem. The other ingredient, which arises naturally in
variational problems, is the idea of a weak solution, namely that we look for residuals which are not pointwise zero,
but rather orthogonal to a set of functions. Often we choose this set based on physical grounds. For example, the
residual could be orthogonal to all low frequency functions, so that any deviation was higher frequency, meaning higher
energy, and thus physically unlikely.

Finite elements arise from a particular choice of functions in which to expand. They have finite support, thus the
\textit{finite}, and that support coincides with cells into which the domain is divided, the \textit{elements}. The
original functions used by Courant and others were linear, or \textit{hat} functions since they look like dunces hats in
one dimension. Today continuous functions which are polynomial when restricted to a cell are popular, and often refered
to as continuous Galerkin elements.

Introduce Ciarlet definition of finite element, namely that I have a triplet $(\mathcal{P}, \mathcal{P}', \mathcal{T})$
where $\mathcal{P}$ is a linear space from which we draw our approximate solution, $\mathcal{P}'$ is the dual space to
$\mathcal{P}$, and $\mathcal{T}$ is the generic cell shape into which the domain $\Omega$ is divided. What this
downplays is that when computing this local definition is paired with conditions between cells. This opens up the
possibility of non-conforming approximations, meaning discrete solution spaces which are not subspaces of the continuum
approximation space.

Show the strong and weak forms Laplace and Stokes equations on the board.

Breakdown weak form computation into pieces. Introduce Jed form of residual and Jacobian.

\section{Lecture 5}

In FEM, we are interested in calculating things like the weak form of the residual
\begin{align}
  \int_\Omega \psi F(u, \nabla u, x, t) \qquad \psi \in P
\end{align}
which can be broken down into cell integrals, and transformed to a reference cell,
\begin{align}
    &\sum_{\mathcal{T}\in\Omega} \int_\mathcal{T} \psi F(u, \nabla u, x, t) \qquad &\psi \in P \\
  = &\sum_{\mathcal{T}\in\Omega} \int_\mathcal{T_\mathrm{ref}} \psi F(u, \nabla u, \xi, t) |J| \qquad &\psi \in P
\end{align}
which can be approximated by quadrature
\begin{align}
  \sum_{\mathcal{T}\in\Omega} \sum_q \psi(x_q) F(u(x_q), \nabla u(x_q), x_q, t) \qquad \psi \in P.
\end{align}
where in all this we have
\begin{align}
  u(x) = \sum_i u_i \phi_i(x) \qquad \phi_i \in P.
\end{align}
Thus it seems advantageous to have the basis function tabulated at the quadrature points, not only to evaluate $\psi$, but
also $u$ and its derivatives.

Another operation we frequently want is the projection of a function onto our approximation space
\begin{align}
  f(x) = \sum_i f_i \phi_i(x).
\end{align}
This is trivial with an orthogonal basis,
\begin{align}
  \int \phi_j f(x) &= \int \phi_j \left( \sum_i f_i \phi_i(x) \right), \\
                   &= \sum_i f_i \left( \int \phi_j \phi_i(x) \right), \\
                   &= \sum_i f_i \delta_{ij}, \\
                   &= f_j.
\end{align}
However, orthogonal bases tend to produce ill-conditioned systems and also often have functions with global support. We
are thus led to a \textit{biorthogonal} system,
\begin{align}
  n_j\left(\phi_i(x)\right) = \delta_{ij} \qquad \phi_i \in P, n_j \in P'.
\end{align}
Now we can get the $i$th finite element coefficient for a function just by acting with the $i$th dual basis vector,
\begin{align}
  n_j(f(x)) &= n_j\left( \sum_i f_i \phi_i(x) \right), \\
            &= \sum_i f_i n_j\left(\phi_i(x)\right), \\
            &= \sum_i f_i \delta_{ij}, \\
            &= f_j,
\end{align}
where we have used the linearity of the functional $n_j$.

FIAT is an automated system for producing a biorthogonal, or \textit{nodal}, basis. The relation is obtained simply from
the biorthogonality requirement. Suppose we already have another basis $m_i$, usually called the \textit{modal} basis,
and that we expand each $\phi_i$ in terms of the $m$ basis,
\begin{align}
  n_j\left(\phi_i(x)\right) &= \delta_{ij}, \\
  n_j\left(\sum_k p^i_k m_k \right) &= \delta_{ij}, \\
  \sum_k p^i_k n_j\left(m_k \right) &= \delta_{ij}.
\end{align}
In matrix notation, this would be
\begin{align}
  V p = I
\end{align}
where the matrix $V$, called the \textit{Vandermonde} matrix, is defined as
\begin{align}
  V_{jk} = n_j\left(m_k \right).
\end{align}
Now we just invert $V$ to get the coefficients $p$ of the nodal basis we want in terms of the original $m$ basis that we
can easily write down. For example, its easy to write down a monomial basis, $x^k$, for any space of polynomials. We
could then use this algorithm to automatically produce the nodal basis. We usually do not use monomials because
evaluation becomes unstable at high degree, but other stable bases, like the Dubiner basis used in FIAT, are available.
Also notice that FIAT automates the production of local basis vectors for the primal and dual spaces, but does not have
an abstraction for the ``piecing together'' of cells.

FIAT also fails to provide an encapsulation of the dual basis vectors; however, this is straightforward. When we
represent the dual basis in our code, we can make use of the Riesz-Markov-Kakutani Representation
Theorem~\parencite{Riesz1909,Markov1938,Kakutani1941,RieszMarkovKakutaniRepresentationTheorem} which says that any
positive linear functional $\psi$  on $C_c(X)$, the space of continuous compactly supported complex-valued functions on
a locally compact Hausdorff space $X$,there is a unique regular Borel measure $\mu$ on $X$ such that
\begin{align}
  \psi(f) = \int_X f(x) d\mu(x).
\end{align}
Since we are operating in the purely discrete world of the computer, we will represent these measures by a quadrature
rule
\begin{align}
  \int_X f(x) d\mu(x) \approx \sum_q w_q f(x_q).
\end{align}
Thus our dual space bases may all be stored internally as sets of quadrature rules supported on a cell and its
boundary.

Sometimes, the specification of $P$ is not so straightforward. For example, we might say that $P$ is the space of all
polynomials of degree $k+2$ with divgerences that are polynomials of degree $k$. It is not easy to write down a basis
for such a space. Thus we start with the bigger space $\bar P = P_{k+2}$ for which we have a nice basis. Next we express
the constraints as functionals. In our example above, we would use a functional like
\begin{align}
  l_i(m_j) = \int \mu^{k-1}_i (\nabla\cdot m_j)
\end{align}
where $\mu^{k-1}_i$ is a Legendre polynomial of degree $k-1$ since this is orthogonal to all polynomials of lesser
degree. Our space $P$ would now be the intersection of the null spaces of all the functionals $l_i$. This is easy to
express using linear algebra. First we act with the functionals on our basis,
\begin{align}
  L_{ij} = l_i(m_j)
\end{align}
and then use the full SVD,
\begin{align}
  L = U \Sigma V.
\end{align}
The last columns of $V$ give a basis for the null space of $L$. We then feed this basis into our algorithm from before
which produces the nodal basis $\phi$.

\paragraph{Homework} Everyone must install the development branch of PETSc from the Git repository. It must be
configured with MPI, SuperLU (so that we have parallel direct solves), Triangle and CTetGen (for unstructured meshing),
p4est (for structured meshing), Metis and Parmetis (for mesh partitioning), and HDF5 (for output). If possible, I
recommend using \href{https://ccache.samba.org/}{ccache} during installation,
e.g. \bashinline{--with-cc="/Users/knepley/MacSoftware/bin/ccache gcc -Qunused-arguments"}, so that recompilation during
development is not so slow.

\paragraph{Quiz} The Neumann problem for the Laplacian can be formulated,
\begin{align}\label{eq:LaplaceNeumann}
  \Delta u = 0 \qquad \int_\Omega u = 0,
\end{align}
where we in essence remove the constant function from the approximation space for $u$. This is also exactly what we see
for the $P_1$ element matrix
\begin{align}
  \begin{pmatrix} 0.5 & 0 & -0.5\\ 0 & 0.5 & -0.5\\ -0.5 & -0.5 & 1.0 \end{pmatrix}
\end{align}
which has the constant vector in its null space since all row sums vanish. However, if we treat the strong form of the
equation~\eqref{eq:LaplaceNeumann} and solve by expanding in a polynomial basis, then the coefficients for the constant
and linear basis functions are both free. What happened to the extra degree of freedom?

\section{Lecture 6}

We start by running 
\begin{bash}
  ./ex12 -run_type test -bc_type dirichlet -interpolate 1 -petscspace_order 1
\end{bash}
which shows the initial guess, $L_2$ error, and initial residual. If we turn on FEM printing,
\begin{bash}
  ./ex12 -run_type test -bc_type dirichlet -interpolate 1 -petscspace_order 1 -dm_plex_print_fem 1
\end{bash}
then it also displays the Jacobian. Increasing the print level
\begin{bash}
  ./ex12 -run_type test -bc_type dirichlet -interpolate 1 -petscspace_order 1 -dm_plex_print_fem 3
\end{bash}
displays each cell integral for the residual and Jacobian, as well as the global matrix indices for each entry. We can
refine the mesh in two different ways. If we use cell volume constraints,
\begin{bash}
  ./ex12 -run_type test -refinement_limit 0.0625 -bc_type dirichlet -interpolate 1 -petscspace_order 1 -dm_plex_print_fem 3
\end{bash}
we can just double the number of cells, and get 5 degrees of freedom (dofs). Whereas regularly refining the mesh
turns each triangle into four subtriangles,
\begin{bash}
  ./ex12 -run_type test -dm_refine 1 -bc_type dirichlet -interpolate 1 -petscspace_order 1 -dm_plex_print_fem 3
\end{bash}
so that we have 9 dofs.

Have them make a small code which calls \cinline{PetscDualSpaceCreateReferenceCell()} for the mesh, and then solves a
problem on the one cell domain. This can really help debugging a new element.

\section{Lecture 10}

We project functions into the finite element space and examine the discrepancy, or $L_2$ error.

\begin{bash}
./fem -dm_refine 0 -dm_view -petscspace_order 1
./fem -dm_refine 2 -dm_view -petscspace_order 1
./fem -dm_refine 4 -dm_view -petscspace_order 1
\end{bash}

\begin{bash}
./fem -dm_refine 4 -dm_view -petscspace_order 2
\end{bash}

\begin{bash}
./fem -dm_refine 1 -dm_view -petscspace_order 0
./fem -dm_refine 1 -dm_view -petscspace_order 0 -petscfe_default_quadrature_order 2
./fem -dm_refine 3 -dm_view -petscspace_order 0 -petscfe_default_quadrature_order 2
./fem -dm_refine 5 -dm_view -petscspace_order 0 -petscfe_default_quadrature_order 2
\end{bash}

\begin{bash}
./fem -dm_refine 0 -dm_view -petscspace_order 1 -solve
./fem -dm_refine 1 -dm_view -petscspace_order 1 -solve
./fem -dm_refine 2 -dm_view -petscspace_order 1 -solve
./fem -dm_refine 2 -dm_view -petscspace_order 2 -solve
\end{bash}

\begin{bash}
./fem -dim 3 -dm_refine 0 -dm_view -petscspace_order 1 -solve
./fem -dim 3 -dm_refine 1 -dm_view -petscspace_order 1 -solve
./fem -dim 3 -dm_refine 2 -dm_view -petscspace_order 1 -solve
./fem -dim 3 -dm_refine 2 -dm_view -petscspace_order 2 -solve
\end{bash}

\subsection{Adding an Implementation Type}

In order to illustrate the process of adding a subtype, we will add a new implementation of the \cinline{PetscDualSpace}
type. First, we need to define the type name
\begin{cprog}
/* Type names for our PetscDualSpace implementations */
#define PETSCDUALSPACEMATT "matt"
\end{cprog}
and a private definition of the underlying data structure
\begin{cprog}
/* Private data for our PetscDualSpace implementations */
typedef struct {
  PetscInt size; /* Just a dummy */
} PetscDualSpace_Matt;
\end{cprog}
Then we may begin the implementation. We will start with mostly empty functions for the basic methods in
\cinline{PetscDualSpace}, but we can see some implementation uses the \cinline{PetscDualSpaceMattGetSize()} method that
we will define below.
\begin{cprog}
PetscErrorCode PetscDualSpaceSetUp_Matt(PetscDualSpace sp)
{
  PetscFunctionBegin;
  PetscFunctionReturn(0);
}

PetscErrorCode PetscDualSpaceDestroy_Matt(PetscDualSpace sp)
{
  PetscFunctionBegin;
  PetscFunctionReturn(0);
}

PetscErrorCode PetscDualSpaceDuplicate_Matt(PetscDualSpace sp, PetscDualSpace *spNew)
{
  PetscInt       order, size;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscDualSpaceCreate(PetscObjectComm((PetscObject) sp), spNew);CHKERRQ(ierr);
  ierr = PetscDualSpaceSetType(*spNew, PETSCDUALSPACEMATT);CHKERRQ(ierr);
  ierr = PetscDualSpaceGetOrder(sp, &order);CHKERRQ(ierr);
  ierr = PetscDualSpaceSetOrder(*spNew, order);CHKERRQ(ierr);
  ierr = PetscDualSpaceMattGetSize(sp, &size);CHKERRQ(ierr);
  ierr = PetscDualSpaceMattSetSize(*spNew, size);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode PetscDualSpaceSetFromOptions_Matt(PetscOptionItems *PetscOptionsObject, PetscDualSpace sp)
{
  PetscInt       size;
  PetscBool      flg;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscDualSpaceMattGetSize(sp, &size);CHKERRQ(ierr);
  ierr = PetscOptionsHead(PetscOptionsObject, "PetscDualSpace Matt Options");CHKERRQ(ierr);
  ierr = PetscOptionsInt("-petscdualspace_matt_size", "Size of element", "PetscDualSpaceMattSetSize", size, &size, &flg);CHKERRQ(ierr);
  if (flg) {ierr = PetscDualSpaceMattSetSize(sp, size);CHKERRQ(ierr);}
  ierr = PetscOptionsTail();CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode PetscDualSpaceGetDimension_Matt(PetscDualSpace sp, PetscInt *dim)
{
  PetscInt       size;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscDualSpaceMattGetSize(sp, &size);CHKERRQ(ierr);
  *dim = size;
  PetscFunctionReturn(0);
}

PetscErrorCode PetscDualSpaceGetNumDof_Matt(PetscDualSpace sp, const PetscInt **numDof)
{
  PetscFunctionBegin;
  *numDof = NULL;
  PetscFunctionReturn(0);
}

PetscErrorCode PetscDualSpaceGetHeightSubspace_Matt(PetscDualSpace sp, PetscInt height, PetscDualSpace *bdsp)
{
  PetscFunctionBegin;
  *bdsp = NULL;
  PetscFunctionReturn(0);
}
\end{cprog}
Now we just need a constructor to build our structure which holds the implementation information, and constructs our
virtual table of functions.
\begin{cprog}
PetscErrorCode PetscDualSpaceInitialize_Matt(PetscDualSpace sp)
{
  PetscFunctionBegin;
  sp->ops->setfromoptions    = PetscDualSpaceSetFromOptions_Matt;
  sp->ops->setup             = PetscDualSpaceSetUp_Matt;
  sp->ops->view              = NULL;
  sp->ops->destroy           = PetscDualSpaceDestroy_Matt;
  sp->ops->duplicate         = PetscDualSpaceDuplicate_Matt;
  sp->ops->getdimension      = PetscDualSpaceGetDimension_Matt;
  sp->ops->getnumdof         = PetscDualSpaceGetNumDof_Matt;
  sp->ops->getheightsubspace = PetscDualSpaceGetHeightSubspace_Matt;
  sp->ops->getsymmetries     = NULL;
  PetscFunctionReturn(0);
}

PETSC_EXTERN PetscErrorCode PetscDualSpaceCreate_Matt(PetscDualSpace sp)
{
  PetscDualSpace_Matt *matt;
  PetscErrorCode       ierr;

  PetscFunctionBegin;
  PetscValidHeaderSpecific(sp, PETSCDUALSPACE_CLASSID, 1);
  ierr     = PetscNewLog(sp, &matt);CHKERRQ(ierr);
  sp->data = matt;

  matt->size = 0;

  ierr = PetscDualSpaceInitialize_Matt(sp);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
\end{cprog}
We need to declare this constructor
\begin{cprog}
PETSC_EXTERN PetscErrorCode PetscDualSpaceCreate_Matt(PetscDualSpace);
\end{cprog}
and we need to register the type in our main program somewhere
\begin{cprog}
/* Register our new implementation types */
ierr = PetscDualSpaceRegister(PETSCDUALSPACEMATT, PetscDualSpaceCreate_Matt);CHKERRQ(ierr);
\end{cprog}

We can have new interface only associated with this type. We declare the function in our header
\begin{cprog}
/* New interface for our PetscDualSpace implementations */
PETSC_EXTERN PetscErrorCode PetscDualSpaceMattGetSize(PetscDualSpace, PetscInt *);
PETSC_EXTERN PetscErrorCode PetscDualSpaceMattSetSize(PetscDualSpace, PetscInt);
\end{cprog}
and then put implementation in the source
\begin{cprog}
static PetscErrorCode PetscDualSpaceMattGetSize_Matt(PetscDualSpace sp, PetscInt *size)
{
  PetscDualSpace_Matt *matt = (PetscDualSpace_Matt *) sp->data;

  PetscFunctionBegin;
  *size = matt->size;
  PetscFunctionReturn(0);
}

PetscErrorCode PetscDualSpaceMattGetSize(PetscDualSpace sp, PetscInt *size)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  PetscValidHeaderSpecific(sp, PETSCDUALSPACE_CLASSID, 1);
  PetscValidPointer(size, 2);
  ierr = PetscTryMethod(sp, "PetscDualSpaceMattGetTensor_C", (PetscDualSpace, PetscInt *), (sp, size));CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
\end{cprog}
Notice that we dynamically dispatch to this function. That way, if it is called on a \cinline{PetscDualSpace} of a
different type, it does nothing instead of failing, which avoids a lot of cumbersome typecasts and branches. Finally, we
must attach these functions in the constructor and detatch them in the destructor
\begin{cprog}
PetscErrorCode PetscDualSpaceDestroy_Matt(PetscDualSpace sp)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscObjectComposeFunction((PetscObject) sp, "PetscDualSpaceMattGetTensor_C", NULL);CHKERRQ(ierr);
  ierr = PetscObjectComposeFunction((PetscObject) sp, "PetscDualSpaceMattSetTensor_C", NULL);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PETSC_EXTERN PetscErrorCode PetscDualSpaceCreate_Matt(PetscDualSpace sp)
{
  PetscDualSpace_Matt *matt;
  PetscErrorCode       ierr;

  PetscFunctionBegin;
  PetscValidHeaderSpecific(sp, PETSCDUALSPACE_CLASSID, 1);
  ierr     = PetscNewLog(sp, &matt);CHKERRQ(ierr);
  sp->data = matt;

  matt->size = 0;

  ierr = PetscDualSpaceInitialize_Matt(sp);CHKERRQ(ierr);
  ierr = PetscObjectComposeFunction((PetscObject) sp, "PetscDualSpaceMattGetSize_C", PetscDualSpaceMattGetSize_Matt);CHKERRQ(ierr);
  ierr = PetscObjectComposeFunction((PetscObject) sp, "PetscDualSpaceMattSetSize_C", PetscDualSpaceMattSetSize_Matt);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
\end{cprog}

\section{Lecture 11}

Suppose we start with reference coordinates $X$, and we refer to components using $X^\alpha$, so that $X^0 = \hat
x$. Then we transform from coordinates $x$ to $X$ using a map $\Phi$,
\begin{align}
  x = \Phi(X),
\end{align}
and we will make frequent use of the infinitesimal version of this mapping
\begin{align}
  J = \frac{\partial x}{\partial X},
\end{align}
for which the matrix form is
\begin{align}
  J^{\alpha\beta} = \frac{\partial x^\alpha}{\partial X^\beta}.
\end{align}
The inverse of the Jacobian is
\begin{align}
  {J^{-1}}^{\alpha\beta} = \frac{\partial X^\alpha}{\partial x^\beta}
\end{align}
so that
\begin{align}
  J^{\alpha\beta} {J^{-1}}^{\beta\gamma} = \frac{\partial x^\alpha}{\partial X^\beta} \frac{\partial X^\beta}{\partial x^\gamma} = \delta_{\alpha\gamma}.
\end{align}

It is easy to see how the gradient transforms
\begin{align}
  \nabla f(x) &= \partial_\alpha f(x) \\
              &= \frac{\partial}{\partial x^\alpha} f(\Phi(X)) \\
              &= \frac{\partial X^\beta}{\partial x^\alpha} \frac{\partial}{\partial X^\beta} f(\Phi(X)) \\
              &= {J^{-1}}^{\beta\alpha} \frac{\partial}{\partial X^\beta} f(\Phi(X)) \\
              &= {J^{-T}}^{\alpha\beta} \frac{\partial}{\partial X^\beta} f(\Phi(X))
\end{align}
which we recognize as the \textit{covariant Piola transform} from~\cite{RognesKirbyLogg2009}, so called because the
gradient is a covariant vector, meaning it transforms like a basis vector rather than a normal vector. We indicate this
in tensor notation by putting the cordinate index as a subscript. Now, how should a vector quantity transform? We know
that the vector itself should remain invariant under coordinate transforms, where we use the Einstein summation
convention,
\begin{align}
  \vv(x)                               &= \vv(X) \\
  f^\alpha(x) dx_\alpha                  &= F^\beta(X) dX_\beta \\
  f^\alpha(x) \frac{}{\partial x^\alpha} &= F^\beta(X) \frac{}{\partial X^\beta} \\
  f^\alpha(x)                           &= \frac{\partial x^\alpha}{\partial X^\beta} F^\beta(X) \\
  f^\alpha(x)                           &= J^{\alpha\beta} F^\beta(X)
\end{align}
which is a contravariant transform. This is not the \textit{contravariant Piola transform} from the paper, because there
they want to transform a special kind of vector, the flux vector. They want conservation of normal flux, and thus we
have to weight by the volume change $|J|$.

The Lie algebra $so(3)$ represents infinitesimal rotations (see
\href{https://en.wikipedia.org/wiki/Rotation_group_SO(3)}{https://en.wikipedia.org/wiki/Rotation_group_SO(3)}), and the
commutation relation comes from the differentiating the orthogonality invariant for group action.

\section{Lecture 12}

PetscSpace: What defines a space?
\begin{itemize}
  \item Order: This is meant to indicate an exponent $\alpha$ such that
\begin{align}
  ||\mathrm{error}|| < C N^{-\alpha}
\end{align}
where $N$ is the number of dofs. Of course, this number will depend on the norm we choose.

  \item Dimension: The size of the basis, summed over components

  \item[ADD] A number of components $Nc$. This is now held by \cinline{PetscFE}, but should be pushed down.

  \item[ADD] A transformation type, such as Vector, 1-form, Flux, 2-form, etc.

  \item \cinline{Evaluate()} returns the basis jet values at the input points.

  \item Polynomial: Tensor Product spaces vs. Max Degree spaces
\end{itemize}

PetscDualSpace: What defines a dual space?
\begin{itemize}
  \item Order: This matches the definition from \cinline{PetscSpace}

  \item DM: The reference cell

  \item Dimension: The size of the basis, summed over components

  \item[ADD] A number of components $Nc$. This is now held by \cinline{PetscFE}, but should be pushed down.

  \item numDof: The number of basis vector assigned to mesh points of a given dimension

  \item Functional: Retrieve a basis vector as a quadrature

  \item Lagrange: conintuity of the basis

  \item Lagrange: tensor product vs. max degree

  \item We also want to be able to retrieve the subspace of this space corresponding to a facet

  \item \cinline{Apply()} returns the action of a basis vector on a function
\end{itemize}

PetscFE: What defines a finite element?
\begin{itemize}
  \item Basis space

  \item Dual space

  \item Spatial dimension, matches reference cell

  \item Cell and face quadrature

  \item Dimension: The size of the basis, summed over cells. If there is a single ref cell, then it should match
    \cinline{PetscSpace} and \cinline{PetscDualSpace}

  \item A number of components $Nc$. This should match \cinline{PetscSpace} and \cinline{PetscDualSpace}.

  \item numDof: The number of basis vector assigned to mesh points of a given dimension, just calls \cinline{PetscDualSpace}.

  \item \cinline{GetTabulation()} returns nodal basis on point set. The default tabulation is on the cell quadrature

  \item Tile sizes for integration

  \item Integration routines for cells and facets, objective, residual, and Jacobian
\end{itemize}

We also discussed explicit examples of vector spaces (2D $P_2$ and 3D $P_1$), calculating the modal primal basis, dual
basis, and nodal primal basis.

We calculated the lowest order RT bases as well.

\section{Lecture 13}

New requirements for assignment
 - Calculate $V$ matrix and nodal basis
 - Calculate interpolants
 - Calculate projections
 - Check error for projection

\printbibliography
\end{document}

TODO:
 - Point evaluation functionals should use weight 0.0 to indicate that its not a 1pt quadrature

Put vector into PetscQuadrature for \int f \cdot v

Push down PetscDualSpaceApply() to impl but have generic tensor one that already exists
 - PetscFE has assumptions about tensor structure when computing sizes. I think we need PetscDualSpace
   to carry a numComponents too. Then we need to change PetscFEGetNumDof()
 - We also need a thorough explainer for sizes starting with Space and DualSpace to take away the tensor assumption

Transformation for Dual dof support functions that are called from Apply impl
 - Is my Apply() function wrong? I do not use |J|, but for point evaluations, it is not needed.
   First, I need to distinguish 1 pt quadrature from point evaluation. Use weight 0.0?
 - Second, in the absence of AD we might need numerical derivatives
   We could also do this by polynomial interpolation

